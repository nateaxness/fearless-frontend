import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeesConferenceForm from './AttendeesConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
// these import statements import the JS forms that we created in the src directory. We can use './xxxxx' because we are importing from the same directory.


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  // function App is checking if the attendees property is undefined. If it is undefined, it returns null.
  // The return statement below returns these React Routes below, if the attendees property is defined. React Routes allows for routing on the front end.
  return (
    <BrowserRouter>
      <Nav />
        <div className="container">
          <Routes>
            <Route index element={<MainPage />} /> {/* how does this work? */}
            <Route path="presentations/new" element={<PresentationForm />} />
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="attendees/new" element={<AttendeesConferenceForm />} />
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          </Routes>
        </div>
    </BrowserRouter>
  );
}
// in React we must export in order to import.
export default App;


// old code:
// import Nav from './Nav';
// import AttendeesList from './AttendeesList';
// import LocationForm from './LocationForm';
// import ConferenceForm from './ConferenceForm';
// import AttendeeForm from './AttendeesForm';

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <>
//       <Nav />
//         <div className="container">
//           {/* <AttendeeForm /> */}
//           {/* <ConferenceForm /> */}
//           {/* <LocationForm /> */}
//           {/* <AttendeesList attendees={props.attendees} /> */}
//         </div>
//     </>
//   );
// }

// export default App;

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <div>
//       Number of attendees: {props.attendees.length}
//     </div>
//   );
// }

// export default App;


// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
