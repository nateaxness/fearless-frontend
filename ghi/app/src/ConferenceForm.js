import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    const [name, setName] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [presentation, setPresentation] = useState('');
    const [attendees, setAttendees] = useState('');
    const [description, setDescription] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
      }

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handlePresentationChange = (event) => {
        const value = event.target.value;
        setPresentation(value);
    }

    const handleAttendeesChange = (event) => {
        const value = event.target.value;
        setAttendees(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = start;
        data.ends = end;
        data.max_presentations = presentation;
        data.max_attendees = attendees;
        data.description = description;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          setName('');
          setStart('');
          setEnd('');
          setPresentation('');
          setAttendees('');
          setDescription('');
          setLocation('');
        }
      }


    const [locations, setLocations] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations)
        console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new conference</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleStartChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                  <label htmlFor="room_count">Starts</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEndChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                  <label htmlFor="city">Ends</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handleDescriptionChange} placeholder="Description" required type="text" name="description" id="description" className="form-control"></textarea>
                  <label htmlFor="city"></label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePresentationChange} placeholder="City" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                  <label htmlFor="city">Maximum presentations</label>
                </div><div className="form-floating mb-3">
                  <input onChange={handleAttendeesChange} placeholder="City" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                  <label htmlFor="city">Maximum attendees</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} required type="text" id="location" name="location" className="form-select">
                    <option defaultValue value="">Choose a location</option>
                    {locations.map(loc => {
                        return (
                            <option key={loc.id} value={loc.id}>
                                {loc.name}
                            </option>
                        )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }

export default ConferenceForm;
